package com.goswych.qrcodetest.ui.home

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.sumimakito.awesomeqr.AwesomeQrRenderer
import com.github.sumimakito.awesomeqr.RenderResult
import com.github.sumimakito.awesomeqr.option.RenderOption
import com.github.sumimakito.awesomeqr.option.background.StillBackground
import com.github.sumimakito.awesomeqr.option.color.Color
import com.github.sumimakito.awesomeqr.option.logo.Logo
import com.goswych.qrcodetest.R


class HomeViewModel : ViewModel() {

    private lateinit var resources: Resources

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val _image = MutableLiveData<Bitmap>()
    val image: LiveData<Bitmap> = _image

    fun initModel(resources: Resources) {
        this.resources = resources
        _image.value = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)
    }

    fun addText(str: String) {
        _text.apply {
            value = "${_text.value}\n$str"
        }

        Log.d("tv", str)
    }

    fun decodeBitmapWithGiveSizeFromResource(
        res: Resources, resId: Int,
        reqWidth: Int, reqHeight: Int
    ): Bitmap {

        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeResource(res, resId, options)

//        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

//         Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeResource(res, resId, options)
    }

    fun calculateInSampleSize(
        options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int
    ): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2
            }
        }
        Log.d("tv", "raw wxh: w=$width, h=$height. Output option.inSampleSize=$inSampleSize")
        return inSampleSize
    }

    fun qrCodeGen(resources: Resources): Bitmap? {
        addText("qrCodeGen")

        val size = 1000

        // A still background (a still image as the background)
        val background = StillBackground()
        background.bitmap = decodeBitmapWithGiveSizeFromResource(resources, R.drawable.app_logo, size, size)
//        background.clippingRect = Rect(0, 0, size, size) // crop the background before applying
        background.alpha = 0.5f // alpha of the background to be drawn

// A blend background (to draw a QR code onto an area of a still image)
//        val background = BlendBackground()
//        background.bitmap = decodeBitmapWithGiveSizeFromResource(resources, R.drawable.app_logo, size, size)
//        background.clippingRect = Rect(0, 0, 200, 200)
//        background.alpha = 0.7f
//        background.borderRadius = 10 // radius for blending corners

// A gif background (animated)
//        val background = GifBackground()
//        background.inputFile = gifFile // assign a file object of a gif image to this field
//        background.outputFile = File(pictureStorage, "output.gif") // IMPORTANT: the output image will be saved to this file object
//        background.clippingRect = Rect(0, 0, 200, 200)
//        background.alpha = 0.7f

        val color = Color()
        color.light = 0xFFFFFFFF.toInt() // for blank spaces
        color.dark = 0xFF2C92C5.toInt() // for non-blank spaces
        color.background = 0xFFFFFFFF.toInt() // for the background (will be overriden by background images, if set)
        color.auto = true // set to true to automatically pick out colors from the background image (will only work if background image is present)

        val logo = Logo()
        logo.bitmap = decodeBitmapWithGiveSizeFromResource(resources, R.drawable.app_logo, 150, 150)
        logo.borderRadius = 10 // radius for logo's corners
        logo.borderWidth = 5 // width of the border to be added around the logo
        logo.scale = 0.3f // scale for the logo in the QR code
//        logo.clippingRect = RectF(0F, 0F, size.toFloat(), size.toFloat()) // crop the logo image before applying it to the QR code

        val renderOption = RenderOption()
        renderOption.content = "https://urlgeni.us/swych-app" // content to encode
        renderOption.size = size // size of the final QR code image
        renderOption.borderWidth = 0 // width of the empty space around the QR code
//        renderOption.ecl = ErrorCorrectionLevel.M // (optional) specify an error correction level
        renderOption.patternScale = .8f // (optional) specify a scale for patterns
        renderOption.roundedPatterns = true // (optional) if true, blocks will be drawn as dots instead
        renderOption.clearBorder = false // if set to true, the background will NOT be drawn on the border area
        renderOption.color = color // set a color palette for the QR code

        //renderOption.background = background // set a background, keep reading to find more about it
        renderOption.logo = logo // set a logo, keep reading to find more about it


        try {
            val result = AwesomeQrRenderer.render(renderOption)
            if (result.bitmap != null) {
                // play with the bitmap
                addText("renderAsync callback")
                _image.apply { value = result.bitmap }


            } else if (result.type == RenderResult.OutputType.GIF) {
                // If your Background is a GifBackground, the image
                // will be saved to the output file set in GifBackground
                // instead of being returned here. As a result, the
                // result.bitmap will be null.
            } else {
                // Oops, something gone wrong.
            }
            return result.bitmap

        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("tv", "renderAsync callback exception ${e.message}")
            // Oops, something gone wrong.
            return null
        }


//        val result = AwesomeQrRenderer.renderAsync(renderOption, { result ->
//            if (result.bitmap != null) {
//
//                // play with the bitmap
//                addText("renderAsync callback")
//                _image.apply { value = result.bitmap }
//
//            } else if (result.type == RenderResult.OutputType.GIF) {
//                // If your Background is a GifBackground, the image
//                // will be saved to the output file set in GifBackground
//                // instead of being returned here. As a result, the
//                // result.bitmap will be null.
//            } else {
//                // Oops, something gone wrong.
//            }
//        }, { exception ->
//            exception.printStackTrace()
//            Log.d("tv", "renderAsync callback exception ${exception.message}")
//            // Oops, something gone wrong.
//        })
    }
}